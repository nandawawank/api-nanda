<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ServiceController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
		$this->load->library('form_validation');
    }

    //Get data from database
    function index_get() {
        
        $username = $this->get('username');
        
        if($username == '') {
            $data = $this->db->get('data')->result();
        }else {
            $this->db->where('username', $username);
            $data = $this->db->get('data')->result();
        }
        
        if($data) {
          $this->response($data, 200);
        }else{
          $this->response("Data not found", 404);  
        }
    }   

    //Insert data to database
    function index_post(){
        
        $config['upload_path']          = './file_upload/';
        $config['allowed_types']        = 'pdf|doc|docx|';
        $config['max_size']             = 1000;
        $this->load->library('upload',$config);
        
        $username = $this->get('username');

        $this->db->like('username',$username);
        $check = $this->db->get('data')->result();                              //check apakah username benar/salah

        if(count($check) != 0) {
            if($this->upload->do_upload('file')) {                              //jika berhasil upload file
                $file_upload = $this->upload->data();
                foreach ($check as $ch) {
                    $data = array(
                        'username' => $ch['username'],
                        'email'    => $ch['email'],
                        'image'    => $file_upload['file_name'],
                        'note'     => 'FALSE'
                    );
                }

                $insert = $this->db->insert('data',$data);

                if($insert) {
                    $this->response($data,200);
                }else {
                    $this->response('Fail insert data to database',404);
                }
            }else{                                                              //jika gagal upload file
                $this->response('Fail upload file',404);
            }
        }else {
            if($this->upload->do_upload('file')) {                              //Jika Berhasil upload file
                $file_upload = $this->upload->data();
                foreach ($check as $ch) {
                    $data = array(
                        'username' => $ch['username'],
                        'email'    => $ch['email'],
                        'image'    => $file_upload['file_name'],
                        'note'     => 'TRUE'
                    );
                }

                $insert = $this->db->insert('data',$data);

                if($insert) {
                    $this->response($data,200);
                }else {
                    $this->response('Fail insert data to database',404);
                }
            }else {                                                             //Jika gagal upload file
                $this->response('Fail upload file',404);
            }
        }
    }
}
?>